import React from 'react';
import master from '../../data/master';

let About =()=>(
    <div className="about">
        <h1>About</h1>
        
        <p>{master.about.summary}</p>
        
    </div>
);

export default About;