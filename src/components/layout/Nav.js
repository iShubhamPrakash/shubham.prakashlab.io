import React from 'react';
import './Nav.css';
import { NavLink } from 'react-router-dom';

let Nav=()=>(
    <div className="nav">
        <div className="toggle-button"><span className="capsule-button"></span></div>
        <ul>
            <li><NavLink to='/' activeClassName="active" className="nav-link"><i class="fas fa-home"></i><span>HOME</span></NavLink></li>
            <li><NavLink to='/projects' activeClassName="active" className="nav-link"><i class="fas fa-laptop-code"></i><span>PROJECTS</span></NavLink></li>
            <li><NavLink to='skills' activeClassName="active" className="nav-link"><i class="fas fa-bolt"></i><span>SKILLS</span></NavLink></li>
            <li><NavLink to='experience' activeClassName="active" className="nav-link"><i class="fas fa-briefcase"></i><span>EXPERIENCE</span></NavLink></li>
            <li><NavLink to='certificates' activeClassName="active" className="nav-link"><i class="fas fa-award"></i><span>CERTIFICATES</span></NavLink></li>
            <li><NavLink to='education' activeClassName="active" className="nav-link"><i class="fas fa-graduation-cap"></i><span>EDUCATION</span></NavLink></li>
            <li><NavLink to='achievements' activeClassName="active" className="nav-link"><i class="fas fa-trophy"></i><span>ACHIEVEMENTS</span></NavLink></li>
            <li><NavLink to='about' activeClassName="active" className="nav-link"><i class="far fa-smile"></i><span>ABOUT</span></NavLink></li>            
            <li><NavLink to='contact' activeClassName="active" className="nav-link"><i class="far fa-handshake"></i><span>HIRE ME</span></NavLink></li>
        </ul>
    </div>
);

export default Nav;