import React from 'react';
import './SideBar.css';
import Nav from './Nav';

let SideBar=()=>(
    <div className="side-bar">
       <Nav/>
    </div>
);

export default SideBar;