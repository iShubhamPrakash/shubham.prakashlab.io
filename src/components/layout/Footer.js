/*
/* IMPORTANT: DO NOT CHANGE THE FOOTER CREDIT
/* You can use this software for free but you have
/* to keep the footer credit unchanged and/or git credit
/* to the creator of this site. 
*/

import React from 'react';
import './Footer.css'

let Footer = ()=>(
    <div className="footer">
        <p 
        style={{color:"rgba(0, 0, 0, 0.3)"}}
        >All Rights Reserved {(new Date()).getFullYear()}  ,
        ©  <a 
            href="https://www.twitter.com/iSuvm"
            style={{textDecoration:"none",color:"rgba(0, 0, 0, 0.3)"}}
            > Shubham Prakash </a> 
              
        </p>
    </div>
);

export default Footer;