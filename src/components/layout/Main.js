import React,{ Component } from 'react';
import { Route } from 'react-router-dom';
import './Main.css';

import About from '../contents/About'
import Home from '../contents/Home'
import Achievements from '../contents/Achievements'
import Certificates from '../contents/Certificates'
import Projects from '../contents/Projects'
import Contact from '../contents/Contact'
import Education from '../contents/Education'
import Experience from '../contents/Experience'
import Skills from '../contents/Skills'

class Main extends Component{
    render(){

        return(
            <div className="main">
                <Route exact path="/" component={Home}/>
                <Route path="/projects" component={Projects}/>
                <Route path="/achievements" component={Achievements}/>
                <Route path="/certificates" component={Certificates}/>
                <Route path="/about" component={About}/>
                <Route path="/contact" component={Contact}/>
                <Route path="/education" component={Education}/>
                <Route path="/experience" component={Experience}/>
                <Route path="/skills" component={Skills}/>
            </div>
        );
    }
}

export default Main;