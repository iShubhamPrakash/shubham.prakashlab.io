import React from 'react';
import { Link } from 'react-router-dom';
import './TopBar.css';
import logo from '../../assets/icons/MyLogo.png';
import master from '../../data/master';


let TopBar = ()=>(
    <div className="top-bar shadow-low">
        <Link to="/" className="top-bar-logo">
            <img src={logo} alt={"logo"} className="logo"/>
        </Link>
        
        <div className="top-bar-name-container">
            <p className="top-bar-name">{master.about.name}</p>
            <p className="top-bar-designation">{master.about.designation}</p>
        </div>
        
        <div className="top-bar-spacer"></div>
        
        <div className="social-link-top-container">
 
            <a href={master.links.github} alt="Github" target="_blank" rel="noopener noreferrer" className="social-link-top"><i className="fab fa-github social-link-top-icon"></i></a>

            <a href={master.links.linkedin} target="_blank" rel="noopener noreferrer" className="social-link-top"><i className="fab fa-linkedin social-link-top-icon"></i></a>

            <a href={master.links.twitter} target="_blank" rel="noopener noreferrer" className="social-link-top"><i className="fab fa-twitter social-link-top-icon"></i></a>

            <a href={master.links.resume} target="_blank" rel="noopener noreferrer" className="social-link-top"><i className="fas fa-arrow-circle-down social-link-top-icon"></i></a>

        </div>
    </div>
)

export default TopBar;