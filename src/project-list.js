// Add the projects as following format

// {
//    title : '',
//    description : '',
//    tag : '',
//    tools : ['abc','def','ghi'],
//    image : 'url or path of image',
//    github_link :  'url of github repo',
//    hosted_link : 'url or hosted project'
// }



const project = [{
        title: 'Project1',
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,",
        tools: ['html', 'css', 'javascript', 'react'],
        image: '',
        github_link: 'https://www.lipsum.com/',
        hosted_link: 'https://www.lipsum.com/'
    },
    {
        title: 'Project2',
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,",
        tools: ['html', 'css', 'javascript', 'react'],
        image: 'https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM5Mjk5fQ',
        github_link: 'https://www.lipsum.com/',
        hosted_link: 'https://www.lipsum.com/'
    },
    {
        title: 'Project3',
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,",
        tools: ['html', 'css', 'javascript', 'react'],
        image: 'https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM5Mjk5fQ',
        github_link: 'https://www.lipsum.com/',
        hosted_link: 'https://www.lipsum.com/'
    },
    {
        title: 'Project4',
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,",
        tools: ['html', 'css', 'javascript', 'react'],
        image: 'https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM5Mjk5fQ',
        github_link: 'https://www.lipsum.com/',
        hosted_link: 'https://www.lipsum.com/'
    },
    {
        title: 'Project5',
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,",
        tools: ['html', 'css', 'javascript', 'react'],
        image: 'https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM5Mjk5fQ',
        github_link: 'https://www.lipsum.com/',
        hosted_link: 'https://www.lipsum.com/'
    },
    {
        title: 'Project6',
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,",
        tools: ['html', 'css', 'javascript', 'react'],
        image: 'https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM5Mjk5fQ',
        github_link: 'https://www.lipsum.com/',
        hosted_link: 'https://www.lipsum.com/'
    },
    {
        title: 'Project7',
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,",
        tools: ['html', 'css', 'javascript', 'react'],
        image: 'https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM5Mjk5fQ',
        github_link: 'https://www.lipsum.com/',
        hosted_link: 'https://www.lipsum.com/'
    },
    {
        title: 'Project8',
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,",
        tools: ['html', 'css', 'javascript', 'react'],
        image: 'https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM5Mjk5fQ',
        github_link: 'https://www.lipsum.com/',
        hosted_link: 'https://www.lipsum.com/'
    }
];

export default project;