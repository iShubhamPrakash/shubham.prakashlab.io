import React, { Component } from 'react';

import TopBar from './components/layout/TopBar';
import SideBar from './components/layout/SideBar';
import Main from './components/layout/Main';
import Footer from './components/layout/Footer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <TopBar/>
        <SideBar/>
        <Main/>
        <Footer/>
      </div>
    );
  }
}

export default App;
